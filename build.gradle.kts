import java.net.URL
import org.gradle.api.tasks.testing.logging.TestExceptionFormat

plugins {
  java
  id("org.openstreetmap.josm") version "0.8.0"
}


repositories {
  mavenCentral()
}
dependencies {
  testImplementation("org.openstreetmap.josm:josm-unittest:SNAPSHOT"){isChanging=true}
  testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.2")
  testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.2")
  // For parsing gtfs files
  packIntoJar("com.opencsv:opencsv:5.6")

  // Can be dropped, once JOSM switches to JUnit 5. This project is only using JUnit 5, but JOSM still requires the vintage engine.
  testImplementation("org.junit.jupiter:junit-jupiter-params:5.8.2")
  testRuntimeOnly("org.junit.vintage:junit-vintage-engine:5.8.2")
  testImplementation("org.awaitility:awaitility:4.2.0")
}

tasks.withType(JavaCompile::class) {
  options.compilerArgs.addAll(
    arrayOf("-Xlint:all", "-Xlint:-serial")
  )
}

tasks.withType(Test::class) {
  useJUnitPlatform()
}

java {
  sourceCompatibility = JavaVersion.VERSION_11
  targetCompatibility = JavaVersion.VERSION_11
  withSourcesJar()
  withJavadocJar()
}

josm {
  josmCompileVersion = "18463"
  pluginName = "visualize-routes"
  manifest {
    author = "Michael Zangl <openstreetmap@michaelzangl.de>"
    mainClass = "org.openstreetmap.josm.plugins.visualizeroutes.VisualizeRoutesPlugin"
    description = "Visualizes routes in the route editor"
    website = URL("https://github.com/michaelzangl/josm-visualize-routes")
    canLoadAtRuntime = true
    minJosmVersion = "17084"
    minJavaVersion = 11
  }
}
